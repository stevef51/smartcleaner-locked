/* This is the Cordova part of things for the index page */
var indexApp = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        httpDaemon.onDeviceReady();
        indexApp.receivedEvent('deviceready');
        navigator.splashscreen.hide();        
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        httpDaemon.start(function(url) {
    		var body = document.querySelector('body');
            angular.bootstrap(body, ['player']);
        });
    }
};

