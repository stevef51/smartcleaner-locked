angular.module('vmBeacons', ['vmngUtils'])

.factory('CRC16', [function() {
    var crc16tab = [
        0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
        0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
        0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
        0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
        0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
        0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
        0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
        0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
        0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
        0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
        0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
        0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
        0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
        0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
        0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
        0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
        0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
        0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
        0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
        0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
        0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
        0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
        0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
        0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
        0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
        0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
        0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
        0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
        0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
        0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
        0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
        0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
    ];
  
    return function(buf, start, len) {
        var crc = 0;
        var j, i;


        for (i = start; i < start + len; i++) {

            var c = buf[i];
            if (c > 255) {
                throw new RangeError();
            }
            j = (c ^ (crc >> 8)) & 0xFF;
            crc = crc16tab[j] ^ (crc << 8);
        }

        return ((crc ^ 0) & 0xFFFF);
    }
}])

.factory('iOSBeaconSvc', ['ngTimeout', 'actionQueue', '$log', '$timeout', 'CRC16', 
    function(ngTimeout, actionQueue, $log, $timeout, CRC16) {
    var base64 = cordova.require('cordova/base64');

    var VirtualMgr201609 = "VirtualMgr201609";
    var VirtualMgr201609_Array = new Uint8Array([0x56, 0x69, 0x72, 0x74, 0x75, 0x61, 0x6c, 0x4d, 0x67, 0x72, 0x32, 0x30, 0x31, 0x36, 0x30, 0x39]);
    var VirtualMgr201609_UUID = '56697274-7561-6C4D-6772-323031363039';
    
    var _bts = cordova.plugins.VirtualManagerBLE;
    var q = actionQueue();

    var arrayCompare = function(array1, start1, array2, start2, length) {
        if (array1.length < start1 + length) {
            return false;
        }

        if (array2.length < start2 + length) {
            return false;
        }

        for(var i = 0 ; i < length ; i++) {
            if (array1[start1 + i] != array2[start2 + i]) {
                return false;
            }
        }
        return true;
    };

    var readInt32 = function(bytes, offset) {
        return (bytes[offset+0] << 24) + (bytes[offset+1] << 16) + (bytes[offset+2] << 8) + (bytes[offset+3]);
    };
    var readUInt32 = function(bytes, offset) {
        return readInt32(bytes, offset) >>> 0;  // Removes sign
    }
    var readInt24 = function(bytes, offset) {
        return (bytes[offset+0] << 16) + (bytes[offset+1] << 8) + (bytes[offset+2]);
    };

    var readInt16 = function(bytes, offset) {
        return (bytes[offset+0] << 8) + (bytes[offset+1]);
    };
    var readUInt16 = function(bytes, offset) {
        return readInt16(bytes, offset) >>> 0;  // Removes sign
    };

    var readInt8 = function(bytes, offset) {
        var b = bytes[offset];
        if (b > 127) {
            b = -(256 - b);
        }
        return b;
    }

    var readUInt8 = function(bytes, offset) {
        return bytes[offset];
    }

    var calculateAccuracy = function(txPower, rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }

        var ratio = rssi*1.0/txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio,10);
        } else {
            var accuracy =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;    
            return accuracy;
        }
    };

    var makeBeaconId = function(i32) {
        var h = (i32 >>> 0).toString(16);
        return ("00000000" + h).substr(h.length);
    }

    var uuidHandler = {}
    uuidHandler[VirtualMgr201609_UUID] = function(peripheral, data) {
        peripheral.beaconId = makeBeaconId(readUInt32(data, 0));
        peripheral.vibrationCount = data[6];
        peripheral.battery = data[7];
        return peripheral;
    };

    uuidHandler['766D'] = function(peripheral, data) {
        var expectedCRC = readInt16(data, 0);
        var calcCRC = CRC16(data, 2, data.length - 2);

        if (expectedCRC != calcCRC) {
            return null;
        }

        peripheral.beaconId = makeBeaconId(readUInt32(data, 2));
        var bpraw = readInt24(data, 6);
        if (bpraw != 0) {
            peripheral.barometricPressure = bpraw / 100.0;
        }

        var idx = 9;
        while(data[idx] != 0 && idx < 22) {
            var code = data[idx++];
            switch(code) {
                case 1: // Battery
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    peripheral.battery = (code-1) * 100/5;
                    break;
                case 7:
                    peripheral.vibrationCount = readUInt16(data, idx);
                    idx += 2;
                    break;
                case 8: // Temperature
                    var raw = readUInt8(data, idx);
                    peripheral.temperature = (raw - 0x80) / 2.0;
                    idx++;
                    break;
                case 9:
                    peripheral.buttonCount = readUInt16(data, idx);
                    idx += 2;
                    break;
            }
        }
        return peripheral;
    };

    var scanningOptions = {
        serviceUUIDs: [VirtualMgr201609_UUID, '766D']
    };
    
    var _rescanTimer = null;
    
    var svc = {
        configure: function(options) {
       		scanningOptions = options.iOS;
        },
        
        startScanning: function(success, failure) {
			_rescanTimer = 'started';                                                                                    
            q(function(next) {
                _bts.startScanning(scanningOptions.serviceUUIDs, { allowDuplicate: true }, ngTimeout(function(peripherals) {
                    // Issue a re-scan every 30 seconds, iOS for some reason slows scan rate down over time
                    if (_rescanTimer == 'started') {
                        _rescanTimer = $timeout(function() {
                        	if (_rescanTimer != 'stopped') {
	                            _rescanTimer = 'started';
    	                        svc.startScanning(success, failure);
                            }
                        }, 30000);
                    }
                    
                    if (peripherals && success) {
                        var beacons = [];
                        angular.forEach(peripherals, function(peripheral) {                            
                            if (peripheral.advertisement && peripheral.advertisement.data) {
                                
                                var data = new Uint8Array(base64.toArrayBuffer(peripheral.advertisement.data));
                                peripheral.advertisement.data = data;
                                
                                var p = uuidHandler[peripheral.advertisement.UUIDS[0]](peripheral, data);
                                if (p) { 
                                    var distance = calculateAccuracy(-60, p.rssi);
                                    p.distance = Math.round(distance * 100.0) / 100.0;

                                    beacons.push(p);
                                }
                            }
                        });
                        if (beacons.length) {
	                    	success(beacons);
                        }
                    }
                }, next), ngTimeout($log.error, failure, next));
            });
        },

        stopScanning: function(success, failure) {
            q(function(next) {
                if (_rescanTimer && _rescanTimer.$state) {
                    $timeout.cancel(_rescanTimer);
                    _rescanTimer = 'stopped';
                }
                
                _bts.stopScanning(
                    ngTimeout(success, next), 
                    ngTimeout($log.error, failure, next));
            });
        },

        getServices: function(peripheral, success, failure) {
            q(function(next) {
                _bts.peripheral_getServices(peripheral.id, ngTimeout(function(services) {
                    peripheral.services = services;
                    if (success) {
                        success(services);
                    }
                }, next), ngTimeout(failure, next));
            });
        }
    }

    q(function(next) {
        _bts.subscribeStateChange(function(state) {
            if (state == 'PoweredOn') {
                next();
            }
        });
    });

    return svc;
}])

.factory('platformBeaconSvc', ['$injector', '$log', function ($injector, $log) {
    if (window.device && window.device.platform) {
        $log.info('Cordova Device = ' + JSON.stringify(window.device));
    	switch(window.device.platform) {
            case 'iOS': 
		        return $injector.get('iOSBeaconSvc');
                
/*            case 'Android':
		        return $injector.get('AndroidBeaconSvc');
*/              
        }
    }
    return {
        notsupported: true
    }
}])

.factory('beaconSvc', ['platformBeaconSvc', '$timeout', '$log', 'rfc4122', function(pbs, $timeout, $log, rfc4122) {
    var _beacons = [];
    var _makeBeaconSignature = function(beacons) {
        var a = beacons.map(function(b) {
            return { id: b.beaconId, distance: b.estimatedDistance };
        });
        return JSON.stringify(a);
    };

	var _subscribers = [];
    
    var _makeSubscribers = function(list, key) {
        return function() {
            var args = Array.prototype.slice.call(arguments);
            list.forEach(function(subscriber) {
                if (subscriber[key]) {
                    subscriber[key].apply(null, args);
                }
            });
        };
    };
    
    var _options = {
        averageCount: 5,
    	beaconExpireSeconds: 10
    }
    
    var _boundDistances = [
        { val: 1, min: 0, max: 1, hmin: 0, hmax: 2, color: '#FF0000' }, 
        { val: 2, min: 1, max: 2, hmin: 0.5, hmax: 4, color: '#C00020' }, 
        { val: 5, min: 2, max: 5, hmin: 1, hmax: 10, color: '#A00040' },
        { val: 10, min: 5, max: 10, hmin: 2, hmax: 20, color: '#800080' },
        { val: 20, min: 10, max: 20, hmin: 5, hmax: 40, color: '#4000A0' },
        { val: 100, min: 20, max: 100, hmin: 10, hmax: 200, color: '#2000C0' },
    	{ val: 200, min: 100,          hmin: 50, color: '#0000FF' }
    ];
    
    var _estimateDistance = function(beacon) {
        // Distance is based on RSSI, if RSSI is bad then we abort
        if (beacon.rssi == 127) {
            return;
        }
        
        // Average the distance first
        beacon.distances = beacon.distances || [];
        beacon.distances.push(beacon.distance);
        if (beacon.distances.length > _options.averageCount) {
            beacon.distances.splice(0, 1);
        }
        beacon.averageDistance = beacon.distances.sum() / beacon.distances.length;

        if (beacon.boundDistance) {
            if (beacon.averageDistance < beacon.boundDistance.hmin) {
                beacon.boundDistance = null;
            } else if (beacon.boundDistance.hmax && beacon.averageDistance > beacon.boundDistance.hmax) {
                beacon.boundDistance = null;
            }
        }
        if (!beacon.boundDistance) {
            for(var i = 0 ; i < _boundDistances.length ; i++) {
                var bd = _boundDistances[i];
                if (beacon.averageDistance >= bd.min) {
                    if (!bd.max || beacon.averageDistance < bd.max) {
	                    beacon.boundDistance = bd; 
                        break;
                    }
                }
            }
        }
        
        beacon.averageDistance = Math.round(beacon.averageDistance);
        beacon.estimatedDistance = beacon.boundDistance.val;
    }
    
    var _startScanning = function() {
        pbs.startScanning(function(peripherals) {
			if (!svc.scanning) {
				return;                                                                                                                            
            }                                                                                    
            svc.scanCount += peripherals.length;
            
            var before = _makeBeaconSignature(_beacons);

            var beacons = _beacons.clone();
            angular.forEach(peripherals, function(peripheral) {
                var existing = beacons.find(function(p) {
                    return p.beaconId == peripheral.beaconId;
                });

                if (!existing) {
                	peripheral.sessionId = rfc4122.v4();
                    beacons.push(peripheral);
                    _estimateDistance(peripheral);                        
                } else {
                    angular.extend(existing, peripheral);
                    _estimateDistance(existing);  
                }
            });

            _beacons.splice(0, _beacons.length);
            _beacons.push.apply(_beacons, beacons);
            _beacons.sort(function(a,b) { 
                return a.beaconId.localeCompare(b.beaconId);
            });

            var after = _makeBeaconSignature(_beacons);

            // Update the last seen timestamp of the beacon, note this is done
            // outside the "before != after" compare above to avoid timestamps
            // continously causing "change" events
            angular.forEach(peripherals, function(peripheral) {
                var beacon = _beacons.find(function(b) {
                    return b.beaconId == peripheral.beaconId;
                });
                if (beacon) {
                    beacon.timestamp = moment();
                }
            });

            if (before != after) {
                _makeSubscribers(_subscribers, 'changed')(_beacons);
            } else {
                _makeSubscribers(_subscribers.filter(function(s) {
					return s.options.anyChange;
				}), 'changed')(_beacons);
			}
        });
    }
        
    var svc = {
        scanning: false,
        
        scanCount: 0,
        
        getBeacons: function() {
            return _beacons;
        },
        
        configure: function(options) {
            if (pbs.notsupported)
                return;

            _options = angular.extend(_options, options.general);
            
            pbs.configure(options);    

            if (svc.scanning) {
                _startScanning();
            }
        },
        
        startScanning: function(changed, options) {
            if (pbs.notsupported)
                return;

			options = options || {
            	forceScan: true                                                                                                             
            };
                                                                                    
			var subscription = {
                options: options,                                                                                              
                changed: changed
            };
            _subscribers.push(subscription);

            if (!svc.scanning && options.forceScan) {
                svc.scanning = true;
                
                _startScanning();
            }
            
            // Calling the returned function will unsubscribe and stopScanning if last subscriber
            return function() {
                _subscribers.remove(function(i) {
                    return i === subscription;
                });

                if (_subscribers.count(function(s) { 
                	return s.options.forceScan == true;
                }) == 0) {
		            svc.scanning = false;
        		    pbs.stopScanning();

		            _beacons.splice(0, _beacons.length);
                }
            }
        },  

        removeExpiredBeacons: function(expireSeconds) {
            if (pbs.notsupported)
                return;

            var before = _beacons.length;
            
            var now = moment();
            var expireTime = moment(now).subtract(expireSeconds, 'seconds');
            var alive = _beacons.filter(function(beacon) {
                return beacon.timestamp.isAfter(expireTime);
            });

            _beacons.splice(0, _beacons.length);
            _beacons.push.apply(_beacons, alive);
            
            var after = _beacons.length;

            if (before != after) {
                _makeSubscribers(_subscribers, 'changed')(_beacons);
            }
        }        
    }
    
    var scheduleNextExpireCheck = function() {
        $timeout(function() {
            svc.removeExpiredBeacons(_options.beaconExpireSeconds);
		
            scheduleNextExpireCheck();
        }, _options.beaconExpireSeconds * 1000 / 2);	// Check twice as often as expire time
    }
    
    if (!pbs.notsupported)
        scheduleNextExpireCheck();
    
    return svc;
}])

.controller('beaconListCtrl', ['$scope', 'beaconSvc', function($scope, beaconSvc) {
	$scope.beacons = beaconSvc.getBeacons();
}])
