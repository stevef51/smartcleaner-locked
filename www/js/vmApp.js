var vmModule = angular.module('player', ['oc.lazyLoad', 
                                                 'ui.router',
                                                 'ui.router.router',
                                                 'ui.router.state',
                                                 'ConsoleLogger',
                                                 'snap',
                                                 'uuid',
                                                 'vmBeacons',
                                                 'bc.AngularKeypad'
                                                 ]);

angular.tryFromJson = function(json) {
    try {
        return angular.fromJson(json);
    } catch (ex) {
        return null;
    }
}
vmModule.config(
    ['$ocLazyLoadProvider', '$provide', '$httpProvider',
     function($ocLazyLoadProvider, $provide, $httpProvider) {
         $httpProvider.defaults.withCredentials = true;
         
// Comment this in if you want to see events from lazy loader in the console
         /*     	$ocLazyLoadProvider.config({
          debug: true
        }); */
         
        $provide.decorator( '$log', [ "$delegate", function( $delegate ) {
			var debugFn = $delegate.debug;
            var logFn = $delegate.log;
            var infoFn = $delegate.info;
            var warnFn = $delegate.warn;
            var errorFn = $delegate.error;
            
            $delegate.messages = [];
            
            $delegate._addMessage = function(level, message) {
                $delegate.messages.push({level: level, message: message});
            }
            
/*          Not going to decorate .debug - this is for Safari debugging only 
			$delegate.debug = function() {
                $delegate._addMessage('debug', arguments[0]);
                
                debugFn.apply(null, arguments);
            }
*/
            $delegate.log = function() {
                $delegate._addMessage('log', arguments[0]);
                
                logFn.apply(null, arguments);
            }
            $delegate.info = function() {
                $delegate._addMessage('info', arguments[0]);
                
                infoFn.apply(null, arguments);
            }
            $delegate.warn = function() {
                $delegate._addMessage('warn', arguments[0]);
                
                warnFn.apply(null, arguments);
            }
            $delegate.error = function() {
                $delegate._addMessage('error', arguments[0]);
                
                errorFn.apply(null, arguments);
            }
            
            return $delegate;
        }]);
     }]
);

// comment this in if you want to see events from the $routing side of things in the console
/*
vmModule.run(['PrintToConsole', function(PrintToConsole) {
    PrintToConsole.active = true;
}]);
*/

vmModule.factory('persistantStorage', ['$timeout', function($timeout) {
    if (window.persistantStorage) {
        var _ps = window.persistantStorage;
        // Make persistantStorage callbacks Angular aware .. 
        var svc = {
            getItem: function(key, success) {
                _ps.getItem(key, success ? function(data) {
                    $timeout(success, 0, true, data);
                } : null);
            },
            setItem: function(key, data, success, failure) {
                _ps.setItem(key, data, success ? function(result) {
                    $timeout(success, 0, true, result);
                } : null, failure ? function(err) {
                    $timeout(failure, 0, true, err);
                } : null);
            },
            removeItem: function(key, success) {
                _ps.removeItem(key, success ? function(exists) {
                    $timeout(success, 0, true, exists);
                } : null);
            },
            clear: function(success) {
                _ps.clear(success ? function() {
                    $timeout(success);
                } : null);
            }
        }
        return svc;
    } else {
        // Revert to localStorage ..
        var svc = {
            getItem: function(key, success) {
                var data = localStorage.getItem(key);
                if (success) {
                    $timeout(success, 0, true, data);
                }
            },
            setItem: function(key, data, success, failure) {
                try {
                    localStorage.setItem(key, data);
                    if (success) {
                        $timeout(success, 0, true, data);
                    }
                } catch (err) {
                    if (failure) {
                        $timeout(failure, 0, true, err);
                    }
                }
            },
            removeItem: function(key, success) {
                var exists = localStorage.getItem(key) != null;
                localStorage.removeItem(key);
                if (success) {
                    $timeout(success, 0, true, exists);
                }
            },
            clear: function(success) {
                localStorage.clear();
                if (success) {
                    $timeout(success);
                }
            }
        }
    }
}]);

// This service is used to manage the local settings on the device (URL etc).
vmModule.factory('vmSettings', ['$http', '$q', '$log', 'rfc4122', 'appVersion', 'appDevice', 'persistantStorage', function ($http, $q, $log, newguid, appVersion, appDevice, persistantStorage) {
	var vmSettingsService = {
        _config:null,
        getConfig: function() {
            var deferred = $q.defer();

            if(vmSettingsService._config != null) {
                deferred.resolve(vmSettingsService._config);
            }
            else {
                $http.get("configuration/AppSettings.json").then(function(json) {
                    $log.debug('App Config loaded as ' + JSON.stringify(json.data));
                    
                    vmSettingsService._config = json.data;
                    deferred.resolve(vmSettingsService._config);
                }, function(reason) {
                    $log.error('An error occurred retrieving the config file');
                    deferred.reject(reason);
                });                    
            }
            
			return deferred.promise;
        },
        getSettings: function(success) {
            persistantStorage.getItem('settings', function(settings) {
                if (success) {                
                    success(JSON.parse(settings) || {});
                }
            });
        },
        setSettings: function(settings, success) {
          while (settings.url.endsWith('/')) {
              settings.url = settings.url.substr(0, settings.url.length-1);
          }
          var schemeUrl = settings.url;
          if (!schemeUrl.startsWith('http')) {
              schemeUrl = 'https://' + schemeUrl;
          }
          if (settings.fullUrl == null || settings.fullUrl === undefined) {
              settings.fullUrl = schemeUrl + '/' + vmSettingsService._config.virtualapp;
          }
          var set = JSON.stringify(settings);
          persistantStorage.setItem('settings', set, function() {
            $log.debug('Settings saved as ' + set);
            if (success) {
                success();
            }
          });
        },
        hasSetting: function(property, cb) {
            vmSettingsService.getSettings(function(settings) {
                if(settings[property] == undefined) {
                    $log.debug(property + ' setting not found');
                    cb(false);
                } else {
                    cb(true);
                }
            });            
        },
        checkUrl: function (url) {
            var deferred = $q.defer();
            var uid = newguid.v4();
            
            if (!url.startsWith('http')) {
                url = 'https://' + url;
            }
            while(url.endsWith('/')) {
                url = url.substr(0, url.length-1);
            }
            
            var fullUrl = url + '/portal/api/v1/Assets/Echo?request=' + uid;
            var r = $http({ method: 'GET', url: fullUrl }).then(function (response) {
                if (response.status == 200) {
                    var jobj = angular.tryFromJson(response.data);
                    if (jobj && jobj.Echo == uid) {
                        deferred.resolve(true);
                        return;
                    }
                }
                // error?
                deferred.reject();
            }, function (error) {
                deferred.reject();
            });
            return deferred.promise;
        },
        getProfile: function(cb) {
            persistantStorage.getItem('TabletProfile', function(profile) {
                cb(profile != null ? JSON.parse(profile) : null);
            });
        },
        updateProfile: function(fullurl) {
            var deferred = $q.defer();
            $http({
                method: 'post',
                url: fullurl + '/API/v1/App/GetProfileForTablet',
                data: {
                    TabletUUID: appDevice.uuid
                }
            }).then(function(response) {
                var fnUpdateTablet = function() {
                    var info = angular.extend({
                        TimeStampUtc: new Date().toUTCString()
                    }, appVersion, appDevice);

                    $http({
                        method: 'post',
                        url: fullurl + '/API/v1/App/UpdateTablet',
                        data: { 
                            TabletUUID: appDevice.uuid,
                            Description: JSON.stringify(info)
                        }
                    }).then(function() {
                        deferred.resolve();
                    }, function(error) {
                        deferred.reject();                    
                    });
                }

                persistantStorage.setItem('TabletProfile', JSON.stringify(response.data || {}), fnUpdateTablet);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }
    };
    
    return vmSettingsService;
}]);

// Service to handle authentication against the server. The URL handed into it
// should be the URL to the root of the site.
vmModule.factory('vmAuthenticate', ['$http', '$q', '$log', 'persistantStorage', function ($http, $q, $log, persistantStorage) {
	var svc = {
        user: {
            username: '',
            authenticated: false,
        },
        loggingIn: false,
        logout: function(success) {
            persistantStorage.removeItem('lastLogin', function() {
                angular.extend(svc.user, {
                    username: '',
                    authenticated: false
                });
                if (success) {
                    success();
                }
            });
        },
        lastLogin: function(cb) {
            persistantStorage.getItem('lastLogin', function(login) {
                cb(login != null ? JSON.parse(login) : null);
            });
        },        
        login: function(url, username, password) {
            var deferred = $q.defer();
            svc.loggingIn = true;
            
            var cancelled = false;
            var canceller = $q.defer();
            var cancel = function(reason) {
                cancelled = true;
                canceller.resolve(reason || 'Cancelled');
            };
            
            $http({
               method: 'post',
               timeout: canceller.promise,
               url: url + '/Authenticate/Login',
               withCredentials: true,
               data: {
                   Username: username,
                   Password: password,
                   IsApiLogin: true
               }
            }).then(function(response) {
	            svc.loggingIn = false;
                if(response.data == 'true') {
                    persistantStorage.setItem('lastLogin', 
                        JSON.stringify({
	                        username: username,
    	                    password: password
                    	}), function() {
                            angular.extend(svc.user, {
                                username: username,
                                authenticated: true
                            });

                            deferred.resolve();
                        });
                }
                else {
                    deferred.reject();
                }
            }, function(error) {
	            svc.loggingIn = false;
                if (cancelled) {
                    deferred.reject({ cancelled: true });
                } else {
                    deferred.reject(error);
                }
            });

            return { 
                promise: deferred.promise,
                cancel: cancel
            };
        }
    };
    
    return svc;
}]);

vmModule.factory('vmVersioning', ['$http', 
                                  '$q',
                                  '$timeout',
                                  '$log',
                                  'persistantStorage',
                                  function ($http, $q, $timeout, $log, persistantStorage) {
	var vmVersioningService = {
		getServerVersionHash: function(fullurl) {
            var deferred = $q.defer();
            
            $http.get(fullurl + '/api/v1/app/VersionHash?nonce=' + Math.random()).then(function(response) {
                deferred.resolve(response.data);
            }, function(reason) {
                $log.log('An error occurred getting the version hash of the server');
                deferred.reject(reason);
            });
            
            return deferred.promise;
        },
        getLocalVersionHash: function(cb) {
            persistantStorage.getItem('resourceversionhash', function(version) {
                cb(version);
            });
        },
        setLocalVersionHash: function(hash, success) {
            $log.info('Setting local version hash to ' + hash);
        	persistantStorage.setItem('resourceversionhash', hash, success);    
        },
		breakOutVersionHash: function(hash) {
            // the different parts of the hash are seperated by \r\n's
            return hash.match(/[^\r\n]+/g);
        },
        breakOutUrl: function(tag, attribute) {
            // so each part of the hash is either a script or a link tag, so we
            // can get at the URL by treating it as XML and extracting the attribute
            // we want, this function helps with that.
            var dom = jQuery.parseXML(tag);
            
            return $(dom.documentElement).attr(attribute);
        },
        requestFileSystem: function() {
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 1024*1024*20, function(fs) {
            	vmVersioningService.fs = fs;
            }, function(error) {
				vmVersioningService.fs = null;
                $log.log('File system request failed');
            });  
        },
        writeFile: function(filename, data, type) {
            var deferred = $q.defer();
            
            if(vmVersioningService.fs == null) {
                // no file system support, in this scenario we don't do anything
                // with the file
                $timeout(function() {
                   deferred.resolve(); 
                });
                
                return deferred.promise;
            }
            
        	var path = cordova.file.dataDirectory;
            
            $log.debug('data directory is ' + path);
            
            window.resolveLocalFileSystemURL(path, function(dirEntry) {
                $log.debug('data directory resolved');
                $log.debug(JSON.stringify(dirEntry));
                
                dirEntry.getFile(filename, {create: true, exclusive: false}, function(fileEntry) {
                   $log.debug('getFile for ' + filename + ' succeeded');
                   $log.debug(JSON.stringify(fileEntry));
                    
                   fileEntry.createWriter(function(writer) {
                       $log.debug('file writer created');
                       writer.onwriteend = function(e) {
                         deferred.resolve();  
                       };
                       writer.onerror = function(e) {
                           deferred.reject(e);
                       };
                       
                       var blob = new Blob([data], { type: type || 'text/plain' });
                       writer.write(blob);
                   }); 
                });
            });
            
            return deferred.promise;
        },
        deleteFile: function(filename) {
            var deferred = $q.defer();
            
            if(vmVersioningService.fs == null) {
                // no file system support, in this scenario we don't do anything
                // with the file
                $timeout(function() {
                   deferred.resolve(); 
                });
                
                return deferred.promise;
            }
            
            var path = cordova.file.dataDirectory;
            
            $log.debug('data directory is ' + path);
            
            window.resolveLocalFileSystemURL(path, function(dirEntry) {
                $log.debug('data directory resolved');
                $log.debug(JSON.stringify(dirEntry));

                dirEntry.getFile(filename, {}, function(fileEntry) {                
                    fileEntry.remove(deferred.resolve, deferred.reject);
                });
            });
            
            return deferred.promise;
        },
        updateResourcesIfRequired: function(fullurl) {
            var deferred = $q.defer();
            
            var deferredSuccess = function() {
                deferred.resolve(httpDaemon.url);
            }
            var deferredFail = function() {
                deferred.resolve('');                
            }
            // first get the server hash
            vmVersioningService.getServerVersionHash(fullurl).then(function(response) {
               	var serverHash = response;
                vmVersioningService.getLocalVersionHash(function(localHash) {
                    $log.info('ServerHash = ' + serverHash);
                    $log.info('LocalHash = ' + localHash);
                    if(serverHash == localHash) {
                        // all good
                        $log.info('Local hash and server hash match');
                        deferred.notify('Local and server versions match');
                        deferred.resolve('');
                        return;
                    }
                    
                    if(vmVersioningService.fs == null) {
                        $log.info('File system not supported, resources will refer to server URL rather than local cache');
                        deferred.notify('File system is not available');
                        deferred.resolve('');
                        return;
                    }
                    
                    // ok, so they're different, we need to download what's on the server
                    $log.info('Local hash and server hash are not equal, starting download and caching of server resources');
                    
                    var skipCache = '';
                    skipCache = 'nonce=' + Math.random();
                    deferred.notify('Downloading App Resources');

                    var fileTransfer = new FileTransfer();
                    var uri = encodeURI(fullurl + '/api/v1/assets/appbundle?' + skipCache);

                    fileTransfer.download(uri, cordova.file.dataDirectory + 'bundle.zip', function(entry) {
                        deferred.notify('Unzipping App Resources');

                        zip.unzip(cordova.file.dataDirectory + 'bundle.zip', cordova.file.dataDirectory, function(zipResult) {
                            if (zipResult == 0) {
                                // We are done, have received and unzipped full bundle
                                vmVersioningService.setLocalVersionHash(serverHash, function() {
                                    deferred.notify('Finished');
                                    vmVersioningService.deleteFile('bundle.zip').then(deferredSuccess, deferredSuccess);
                                });                                                
                            } else {
                                deferred.notify('Zip error ' + zipResult);
                                vmVersioningService.deleteFile('bundle.zip', deferredFail, deferredFail);
                            }
                        });
                        
                    }, function() {
                        deferred.notify('Downloading JS Resources');
                        // There is no ZIP Bundle, revert to seperate JS, CSS and HTML Template bundles ..
                        $http.get(fullurl + '/api/v1/assets/jsbundle?id=jsbundle_ios&' + skipCache).then(function(jsResponse) {
                            $log.info('JS bundle retrieved from server');
                            vmVersioningService.writeFile('bundle.js', jsResponse.data).then(function() {
                                $log.info('JS bundle saved to local file system, starting retrieval of CSS bundle');
                                deferred.notify('Downloading CSS Resources');
                                
                                $http.get(fullurl + '/api/v1/assets/cssbundle?id=cssbundle_ios&' + skipCache).then(function(cssResponse) {
                                    $log.info('CSS bundle retrieved from server');
                                    vmVersioningService.writeFile('bundle.css', cssResponse.data).then(function() {
                                        $log.info('CSS bundle saved to local file system, starting retrieval of HTML templates');
                                        $http.get(fullurl + '/api/v1/assets/templatebundle?' + skipCache).then(function(templateResponse) {
                                            $log.log('HTML template bundle retrieved from server');
                                            vmVersioningService.writeFile('bundle.html', templateResponse.data).then(
                                            function() {
                                                $log.info('HTML template bundle saved to local file system, all resources have been downloaded and cached');
                                                vmVersioningService.setLocalVersionHash(serverHash, function() {
                                                    deferred.notify('Finished');
                                                    deferred.resolve(baseUrl);
                                                });                                                
                                            }, function(templateError) {
                                                $log.error('An error occurred writing the template bundle to the device file api');
                                                deferred.reject(templateError);
                                            });
                                        }, function(templateError) {
                                            $log.error('An error occurred getting the template bundle from the server');
                                            deferred.reject(templateError);
                                        });
                                    }, function(cssError) {
                                        $log.error('An error occurred writing the css bundle to the device file api');
                                        deferred.reject(cssError);
                                    });
                                }, function(reason) {
                                    $log.error('An error occurred getting the CSS bundle from the server');
                                    deferred.reject(reason);
                                });
                            }, function(error) {
                                $log.error('An error occurred writing the js bundle to the device file api');
                                deferred.reject(error);
                            });
                        }, function(reason) {
                            $log.log('An error occurred getting the js bundle from the server');
                            deferred.reject(reason);
                        });
                    });
                });
            }, function(reason) {
                $log.log('An error occurred getting the version hash from the server');
                deferred.reject(reason);                
            });

            return deferred.promise;
        },
        jsBundleUrl: function(fullurl) {
            if(vmVersioningService.fs == null || angular.isDefined(vmVersioningService.fs) == false) {
                // let's use the live URL (this should only really happen in testing in a browser)
                return fullurl + '/api/v1/assets/jsbundle?id=jsbundle_ios';
            }
            else {
                return cordova.file.dataDirectory + 'bundle.js';
            }
        },
        cssBundleUrl: function(fullurl) {
            if(vmVersioningService.fs == null || angular.isDefined(vmVersioningService.fs) == false) {
                // let's use the live URL (this should only really happen in testing in a browser)
                return fullurl + '/api/v1/assets/cssbundle?id=cssbundle_ios';
            }
            else {
                return cordova.file.dataDirectory + 'bundle.css';
            }
        },
        templateBundleUrl: function(fullurl) {
            if(vmVersioningService.fs == null || angular.isDefined(vmVersioningService.fs) == false) {
                // let's use the live URL (this should only really happen in testing in a browser)
                return fullurl + '/api/v1/assets/templatebundle';
            }
            else {
                return cordova.file.dataDirectory + 'bundle.html';
            }
        },
        templateBundle: function(fullurl) {
            var deferred = $q.defer();
            
            if(vmVersioningService.fs == null || angular.isDefined(vmVersioningService.fs) == false) {
				// we download the bundle from the server in this instance
            	$http.get(fullurl + '/api/v1/assets/templatebundle').then(function(templateResponse) {
                	deferred.resolve(templateResponse.data);    
                }, function(templateError) {
                    $log.log('An error was encountered downloading the template bundle from the server');
                    deferred.reject(templateError);
                });
            }
            else {
				// we have it local, so get it from there
                
            }
            
            return deferred.promise;
        }
    };
                          
    vmVersioningService.requestFileSystem();
                                      
    return vmVersioningService;
}]);

vmModule.factory('spinnerDialog', [function() {
    return (window.plugins ? window.plugins.spinnerDialog : null) || {
        show: function() {            
        },
        hide: function() {            
        }        
    }
}]);

vmModule.factory('appDevice', [function() {
    var svc = angular.extend({}, window.device);

    svc.uuid = svc.uuid.toUpperCase();

    return svc;
}]);

vmModule.factory('appVersion', [function() {
    var svc = {
        appName: 'VM Player', 
        versionNumber: 'Version',
        versionCode: 'Code',
        packageName: 'Package'
    };
    
    if (cordova && cordova.getAppVersion) {
        cordova.getAppVersion.getAppName(function(value) { 
			svc.appName = value;                                                   
		});
        cordova.getAppVersion.getVersionNumber(function(value) { 
			svc.versionNumber = value;                                                   
		});        
        cordova.getAppVersion.getVersionCode(function(value) { 
			svc.versionCode = value;                                                   
		});        
        cordova.getAppVersion.getPackageName(function(value) { 
			svc.packageName = value;                                                   
		});        
    }
    
    return svc;
}])

// This controller handles the movement through the various states required for the app
// to run. These follow the following workflow.
//    1. URL - A Url must be supplied if one isn't stored in settings. If a URL is present in settings, we go to step 2
//    2. Authentication - The user must authenticate against the URL defined in 1.
//    3. Test resource versions - We test the server to make sure the JS and CSS we have for the main part of the app are up to date
//        3A. If resources aren't up to date or we don't have them, then download them and store in the local file system
//    4. Steps 1,2,3 are all good, ok navigate off to eh app.html page, which will handle things from here
vmModule.controller('loaderController', ['$scope',
                                         '$rootScope', 
                                         'vmSettings',
                                         'vmAuthenticate',
                                         'vmVersioning',
                                         '$q',
                                         '$log', 
                                         '$window', 
                                         'spinnerDialog',
                                         'appVersion',
                                         '$timeout',
                                         '$http',
                                         '$filter',
                                         'appDevice',
                                         'persistantStorage',
                                         'snapRemote',
                                         loaderController]);
function loaderController($scope, $rootScope, vmSettings, vmAuthenticate, vmVersioning, $q, $log, $window, spinnerDialog, appVersion, $timeout, $http, $filter, appDevice, persistantStorage, snapRemote) {
    spinnerDialog.hide();

    $scope.appVersion = appVersion;    
    $scope.appDevice = appDevice;

    $scope.auth = vmAuthenticate;
    vmAuthenticate.lastLogin(function(data) {
        $scope.login = data || {};

        // Try cached last login ..
        if ($scope.login && $scope.login.username) {
    //        alert('STOP 1 - Attach Debugger');
            $timeout(function() {
    //            debugger;
                $scope.loginNow($scope.login.username, $scope.login.password);
            }, 1000);
        }
    });

    vmSettings.getSettings(function(settings) {
        $scope.settings = settings;
    });
    
    vmSettings.getProfile(function(tabletProfile) {
        $scope.tabletProfile = tabletProfile;
        $scope.allowNumberPadLogin = tabletProfile && tabletProfile.allownumberpadlogin;
        $scope.showNumberpad = $scope.allowNumberPadLogin;
    });

    $scope.toggleNumberpad = function() {
        $scope.showNumberpad = !$scope.showNumberpad;
    }
    $scope.state = 'url';
    
    $log.debug('load controller loaded with ' + JSON.stringify($scope.settings));
    
    $scope.showConsole = false;
    
    $scope.resourceMessages = [];
    
    // just pre-load the application config (makes debugging issues a little easier)
    vmSettings.getConfig();
    
    $scope.calculateState = function() {
        vmSettings.hasSetting('url', function(has) {
            $scope.state = has ? 'login' : 'url';
        });
    };
 
    $scope.$on('saved', function() {
        $scope.calculateState();            
    });
    
    $scope.loginNow = function(username, password) {        
        $log.info('Attempting to login to ' + $scope.settings.fullUrl);
        
    	$scope.login.failed = false;
        
        var fnError = function(error) {
            // failure
            $scope.state = 'login';
            spinnerDialog.hide();

            $log.error('Login to ' + $scope.settings.fullUrl + ' failed');
            if (!error || !error.cancelled) {
                $scope.login.failed = true;
                $scope.login.usernumber = '';
            }
        };

        var loginRequest = vmAuthenticate.login($scope.settings.fullUrl, username, password);
        spinnerDialog.show('Logging in', null, loginRequest.cancel);
        loginRequest.promise.then(
            function() {
                var fnCheckResources = function() {
                    $scope.state = 'checkresources';
                    vmVersioning.updateResourcesIfRequired($scope.settings.fullUrl).then(
                        function(result) {
                            $scope.state = 'loggedin';
                            window.location = result + 'dashboard.html';
                        }, fnError, function(update) {
                            $scope.resourceMessages.push(update);
                        });
                }

                // Attempt TabletProfile, fail quietly (server is not TabletProfile enabled)
                vmSettings.updateProfile($scope.settings.fullUrl).then(fnCheckResources, fnCheckResources);
        	}, fnError);
    };
    
    $scope.refresh = function() {
    	location.reload();    
    }
    
    $scope.logout = function() {
		// Show the login page  
        persistantStorage.removeItem('user', function() {            
            $scope.state = 'login';
            window.location = 'index.html';
        });
    };
    
    $scope.gotoSettings = function() {
        $scope.state = 'url';
    };
    
    $scope.toggleConsole = function() {
      $scope.showConsole = !$scope.showConsole;  
    };
    
    $scope.showUrl = function() { 
    	$scope._showUrl = true;
    }
    
    $scope._showUrl = false;
    $scope.calculateState();

    $scope.inKioskMode = false;

    if (window.KioskPlugin) {
        snapRemote.getSnapper().then(function(snapper) {
            snapper.on('open', function() {
                window.KioskPlugin.isSetAsLauncher(function(inKiosk) {
                    $timeout(function() {
                        $scope.inKioskMode = inKiosk;
                    });
                });
            });
        });
    }

    $scope.exitKioskMode = function() {
        if ($scope.tabletProfile && $scope.tabletProfile.exitKioskModePassword) {
            if (prompt('Enter tablet profile password to exit Kiosk mode') == $scope.tabletProfile.exitKioskModePassword) {
                window.KioskPlugin.exitKiosk();
            }
        } else {
            if (prompt('Enter password to exit Kiosk mode') == 'k!osk123') {
                window.KioskPlugin.exitKiosk();
            }
        }
    }
}

// This controller handles the movement through the various states required for the app
// to run. These follow the following workflow.
//    1. URL - A Url must be supplied if one isn't stored in settings. If a URL is present in settings, we go to step 2
//    2. Authentication - The user must authenticate against the URL defined in 1.
//    3. Test resource versions - We test the server to make sure the JS and CSS we have for the main part of the app are up to date
//        3A. If resources aren't up to date or we don't have them, then download them and store in the local file system
//    4. Steps 1,2,3 are all good, ok navigate off to eh app.html page, which will handle things from here
vmModule.controller('dashboardController', ['$scope',
                                         '$rootScope', 
                                         'vmSettings',
                                         'vmAuthenticate',
                                         'vmVersioning',
                                         '$q',
                                         '$log', 
                                         '$window', 
                                         'spinnerDialog',
                                         'appVersion',
                                         'appDevice',
                                         '$http',
                                         dashboardController]);
function dashboardController($scope, $rootScope, vmSettings, vmAuthenticate, vmVersioning, $q, $log, $window, spinnerDialog, appVersion, appDevice, $http) {
    spinnerDialog.hide();

    $scope.appVersion = appVersion;    
    $scope.appDevice = appDevice;

    $scope.auth = vmAuthenticate;
    vmSettings.getSettings(function(settings) {
        $scope.settings = settings;

        $http.get(settings.fullUrl + '/api/v1/app/webappversion?nonce=' + Math.random()).then(function (data) {
            appVersion.webAppVersion = data.data;
        });
    });
    
    var _tabletProfile = {};
    vmSettings.getProfile(function(tabletProfile) {
        _tabletProfile = tabletProfile || {};
    });

    // just pre-load the application config (makes debugging issues a little easier)
    vmSettings.getConfig();
    
    $scope.refresh = function() {
    	location.reload();    
    }
    
    $scope.logout = function(ignoreDialog) {
        if (!ignoreDialog) {
            switch(_tabletProfile.confirmlogout) {
                case "SimpleConfirm":
                    if (!confirm("Are you sure you want to logout?")) {
                        return;
                    }
                    break;
                
                default:
                    break;
            }
        }

		// Show the login page  
        vmAuthenticate.logout(function() {        
            window.location = 'index.html';
        });
    };

    $rootScope.$on('auto-logout.execute', function(event, args) {
        $scope.logout(true);
    })
    

}

vmModule.controller('settingsController', ['$scope', 
                                         'vmSettings',
                                         'vmAuthenticate',
                                         'vmVersioning',
                                         '$q',
                                         '$log',
                                         '$window',
                                         settingsController]);
function settingsController($scope, vmSettings, vmAuthenticate, vmVersioning, $q, $log, $window) {
	vmSettings.getSettings(function(settings) {
        $scope.settings = settings; 
    });
 	$scope.showConsole = false;
    
    $scope.saveUrl = function() {
        vmSettings.getConfig().then(function(config) {
            $scope.settings.fullUrl = $scope.settings.url + '/' + config.virtualapp;
            vmSettings.setSettings($scope.settings, function() {
            	window.location = 'index.html';           
            });
        });
    };
    
    $scope.toggleConsole = function() {
      $scope.showConsole = !$scope.showConsole;  
    };
}

vmModule.directive('logViewer', ['$log',
   function($log) {
       return {
           templateUrl: 'templates/log-viewer.html',
           restrict: 'E',
           replace: true,
           transclude: true,
           link: function(scope, elt, attrs) {
				scope.log = $log;
           }
       };
   } 
]);

vmModule.directive('settingsUrl', ['$timeout', '$log', 'vmSettings',
   function($timeout, $log, vmSettings) {
       return {
           templateUrl: 'templates/settings-url.html',
           restrict: 'E',
           replace: true,
           transclude: true,
           require: 'ngModel',
           scope: {
               settings: '=ngModel'
           },
           controller: function($scope) {
           		$scope.state = 'unknown';

                $scope.checkUrl = function () {
                    $scope.state = 'checking';

                    vmSettings.checkUrl($scope.settings.url).then(function () {
                        $scope.state = 'valid';
                    }, function () {
                        $scope.state = 'invalid';
                    });
                }

                var debounceCheckUrl = $scope.checkUrl.debounce(2000);

                $scope.$watch('settings.url', function (newvalue, oldvalue) {
                    if (newvalue) {
                        $scope.state = 'changing';
                        debounceCheckUrl();
                    } else {
                        debounceCheckUrl.cancel();
                        $scope.state = 'invalid';
                    }
                });
               
               $scope.clearUrl = function() {
                   $scope.settings.url = '';
               }
               
               $scope.scan = function() {
                   cordova.plugins.barcodeScanner.scan(
                       function(result) {
                           $timeout(function() {
                               $scope.settings.url = result.text;
                               if ($scope.settings.url.startsWith('http://')) {
                                   $scope.settings.url = $scope.url.substr(7);
                               } else if ($scope.settings.url.startsWith('https://')) {
                                   $scope.settings.url = $scope.settings.url.substr(8);
                               }
                           });
                       },
                       function(error) {
                           
                       },
                   	   {
                        	"preferFrontCamera" : false, // default false
                        	"showFlipCameraButton" : true // default false
                   	   }
                   );
               }
               
               $scope.saveUrl = function(success) {
                   $scope.settings.fullUrl = null;
                   vmSettings.setSettings($scope.settings, function() {
                        $scope.$emit('saved');
                        if (success) {
                            success();
                        }
                   });
               }
           }
       };
   } 
]);

vmModule.controller('dashboardListener', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.dashboards = [];
    
    $rootScope.$on('dashboards', function(event, args) {
        $scope.dashboards = args.dashboards;
    })
}]);

vmModule.controller('beaconListener', [
'$scope', 'beaconSvc', '$geolocation', '$log', '$timeout', 'bworkflowApi', 'appDevice', 'persistantStorage',
function($scope, beaconSvc, $geolocation, $log, $timeout, bworkflowApi, appDevice, persistantStorage) {
    // Record handling
    $scope.records = [];       
    
    var _lastRecord = null;
    var makeRecord = function() {
        var position = $scope.position;
        var beacons = beaconSvc.getBeacons();
        
        var record = {
            position: angular.copy(position),
            beacons: beacons.map(function(b) {
                return { 
                    beaconId: b.beaconId, 
                    sessionId: b.sessionId,
                    distance: b.estimatedDistance,
                    timestamp: b.timestamp
                };
            })
        }
        
        // Make sure we dont queue the exact same record in a row (this may happen since we have 2 triggers (GPS & Beacons))
        var thisRecord = JSON.stringify(record);
        if (thisRecord != _lastRecord) {
	        $scope.records.push(record);
            _lastRecord = thisRecord;
        }
    }.lazy(3000, false, 1);			// Delay for 3s and limit to 1 call per 3 second
    
    // Beacons
    $scope.beacons = beaconSvc.getBeacons();  
    $scope.beaconSvc = beaconSvc;
    $scope.scanning = null;

    $scope.$watch('beaconSvc.scanCount', function() {
        $('.ble_flasher').removeClass('ble_receive');
        $timeout(function() {
            $('.ble_flasher').addClass('ble_receive');
        }, 0);
    });

    var serviceOptions;
    var executeServiceOptions = function() {
        if (!serviceOptions) {
            return;
        }
        beaconSvc.configure(serviceOptions);

        if (serviceOptions.enableBLEScanning) {
            $scope.startScanning();
            startGPS();
        } else {
            $scope.stopScanning();
            stopGPS();
        }
    }

    var getServerServiceOptions = function() {
        bworkflowApi.execute('AssetTracker', 'GetServiceOptions')
            .then(function(data) {
                serviceOptions = data;
                persistantStorage.setItem('beaconServiceOptions', JSON.stringify(data));

                executeServiceOptions();
            });    
    };

    persistantStorage.getItem('beaconServiceOptions', function(data) {
        if (data) {
            serviceOptions = JSON.parse(data);
            executeServiceOptions();
        }

        getServerServiceOptions();
    });

    $scope.startScanning = function() {
        if (!$scope.scanning) {
            $scope.scanning = beaconSvc.startScanning(function(beacons) {
                if ($scope.position && !$scope.position.error) {
                    makeRecord();
                }
            });
        }
    }
    
    $scope.stopScanning = function() {
        if ($scope.scanning) {
	        $scope.scanning();
	        $scope.scanning = null;
        }
    }
    
    // Geolocation
    var geooptions = {
//        timeout: 60000,
//        maximumAge: 250,
        enableHighAccuracy: true
    };

    var startGPS = function() {
	    $geolocation.watchPosition(geooptions);        
    };
    var stopGPS = function() {
        $geolocation.clearWatch();
    };

    $scope.$on('$geolocation.position.error', function(event, args) {
		startGPS();        // Try again
    });
    $scope.$on('$geolocation.position.changed', function(event, args) {
        try {
            if (!$scope.position 
                || $scope.position.coords.latitude != args.coords.latitude 
                || $scope.position.coords.longitude != args.coords.longitude
                || $scope.position.coords.accuracy != args.coords.accuracy)
            {
                $scope.position = {
                    coords: {
                        accuracy: args.coords.accuracy,
                        altitude: args.coords.altitude,
                        altitudeAccuracy: args.coords.altitudeAccuracy,
                        heading: args.coords.heading,
                        latitude: args.coords.latitude,
                        longitude: args.coords.longitude,
                        speed: args.coords.speed
                    },
                    timestamp: args.timestamp
                };

                makeRecord();
            }
        } catch(err) {
            $log.error(err);
        }
    });
      
    // Server messaging
    var scheduleNextSendToServer = function(interval) {
        sendToServer.nextInterval = interval || sendToServer.nextInterval || 30000;
        if (sendToServer.timeout) {
			$timeout.cancel(sendToServer.timeout);
		}
		sendToServer.timeout = $timeout(sendToServer, sendToServer.nextInterval);
    };
    
    var sendToServer = function() {
        if ($scope.records.length) {
            var records = $scope.records;
            $scope.records = [];
            
            bworkflowApi.execute('AssetTracker', 'StoreGPSBeaconData', { 
                tabletUUID: appDevice.uuid,
                records: records })
            .then(function(data) {
                // Note, response contains when to schedule next send
                scheduleNextSendToServer(data.nextInterval);
            });
        }
		scheduleNextSendToServer();
    };
    scheduleNextSendToServer(10000);
}]);





