/* This is the Cordova part of things for the index page */
var bootApp = {
    // Application Constructor
    boot: function () {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function () {
        navigator.splashscreen.hide();

        httpDaemon.onDeviceReady();

        // The httpDaemon is setup to serve resources from the following folders in order
        // 1. PersistantStorage (writable)
        // 2. The WWW folder (readonly)
        //
        // By doing this we essentially have made the readonly WWW folder read-write, since we can write
        // override files to the PersistantStorage and they will get served from there, allowing us to 
        // override *any* file in WWW (except bootApp.js and index.html since they are always served from Cordova's file system)
        httpDaemon.start(function (url) {
            var start = function () {
                var appUrl = url + 'default.html';

                window.location = appUrl;
            }

            var debug = false;
            if (debug) {
                alert('Attach debugger');

                window.setTimeout(function () {
                    debugger;

                    window.setTimeout(start, 2000);
                }, 10000);
            } else {
                start();
            }
        });
    },
};

