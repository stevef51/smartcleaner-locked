document.addEventListener('deviceready', function() {
    if (!window.persistantStorage) {
        var timeout = function(fn, delay) {
            window.setTimeout(fn, delay);
        }

        var svc = {
            NATIVE_WRITE_FAILED: 1,
            ITEM_NOT_FOUND: 2,
            NULL_REFERENCE: 3,
            UNDEFINED_TYPE: 4,
            JSON_ERROR: 5,
            WRONG_PARAMETER: 6
        };

        if (window.requestFileSystem) {
            var DIRECTORY = cordova.file.dataDirectory;
            var FILENAME = 'localStorage.json';
            var _fs = null;
            var _ls = null;

            var _save = function(data, success, failure) {
                success = success || function() {};
                failure = failure || function() {};
                window.resolveLocalFileSystemURL(DIRECTORY, function(dirEntry) {
                    dirEntry.getFile(FILENAME, {create: true, exclusive: false}, function(fileEntry) {
                    fileEntry.createWriter(function(writer) {
                        writer.onwriteend = function(e) {
                            success();
                        };
                        writer.onerror = function(e) {
                            failure(e);
                        };
                        
                        var blob = new Blob([data], { type: 'text/plain' });
                        writer.write(blob);
                    }); 
                    });
                });

            }

            var _load = function(fn) {
                window.resolveLocalFileSystemURL(DIRECTORY, function(dirEntry) {
                    dirEntry.getFile(FILENAME, {}, function(fileEntry) {
                        // Get a File object representing the file,
                        // then use FileReader to read its contents.
                        fileEntry.file(function(file) {
                        var reader = new FileReader();

                        reader.onloadend = function(e) {
                            fn(this.result);
                        };

                        reader.readAsText(file);
                        }, function(error) {
                            fn(null);
                        });
                    }, function(error) {
                        fn(null);
                    });
                }, function(error) {
                    fn(null);
                });
            }    

            var _deferredQ = null;
            
            var getLS = function(cb) {
                if (_ls == null) {
                    if (_deferredQ == null) {
                        _deferredQ = [cb];
                        _load(function(text) {
                            try {
                                _ls = JSON.parse(text) || {};
                            } catch(err) {
                                _ls = {};
                            }
                            _deferredQ.forEach(function(d) {
                                d();
                            });
                            _deferredQ = [];
                        })
                    } else {
                        _deferredQ.push(cb);
                    }
                } else {
                    cb();
                }
            }

            var saveLS = function(success, failure) {
                var json = JSON.stringify(_ls);
                _save(json, success, failure);
            }

            svc.getItem = function(key, success) {
                getLS(function() {
                    var data = _ls[key];
                    // localStorage returns null for not found
                    if (data === undefined) {
                        data = null;
                    }
                    timeout(function() {
                        success(JSON.parse(JSON.stringify(data)));
                    }, 0);
                });
            };

            svc.setItem = function(key, value, success, failure) {
                getLS(function() {
                    _ls[key] = value;
                    saveLS(success, failure);
                });
            };

            svc.removeItem = function(key, success) {
                getLS(function() { 
                    var data = _ls[key];
                    if (data !== undefined) {
                        delete _ls[key];                    
                        saveLS(function() { 
                            if (success) {
                                success(true);
                            }
                        });
                    } else if (success) {                        
                        timeout(function() { 
                            success(false);
                        }, 0);
                    }
                });
            },

            svc.clear = function(success) {
                getLS(function() {
                    _ls = [];
                    saveLS(success);
                });
            };
        } else { 
            svc.getItem = function(key, success) {
                var v = localStorage.getItem(key);
                if (success) {
                    timeout(function() { 
                        success(v);
                    }, 0);
                }
            };

            svc.setItem = function(key, value, success, failure) {
                try {
                    localStorage.setItem(key, value);
                    if (success) {
                        timeout(function() { 
                            success(value);
                        }, 0);
                    }
                } catch (err) {
                    if (failure) {
                        timeout(function() { 
                            failure(err);
                        }, 0);
                    }
                }
            };

            svc.removeItem = function(key, success) {
                var exists = localStorage.getItem(key) != null;
                localStorage.removeItem(key);
                if (success) {
                    timeout(function() { 
                        success(exists);
                    }, 0);
                }
            };

            svc.clear = function(success) {
                localStorage.clear();
                if (success) {
                    timeout(function() { 
                        success();
                    }, 0);
                }
            };
        }
        window.persistantStorage = svc;
    }
}, false);
